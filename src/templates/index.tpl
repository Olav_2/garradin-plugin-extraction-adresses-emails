{include file="admin/_head.tpl" title="Extension — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id js=1}

<p>Extraction des emails de membres en vue d'envoyer des emails de groupe avec un logiciel de messagerie ou un webmail.</p>
<br>

<table class="list">
	<thead>
		<tr>
			<th>Nom de la recherche</th>
			<th>Type</th>
			<th>Statut</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$liste item="recherche"}
		<tr>
			<th>{$recherche.intitule}</th>
			<td>{if $recherche.type == Recherche::TYPE_JSON}Avancée{else}SQL{/if}</td>
			<td>{if !$recherche.id_membre}Publique{else}Privée{/if}</td>
			<td class="actions">
				<a href="index.php?id={$recherche.id}" class="icn" title="Liste des emails">&#9993;</a>
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>

{if !empty($recherche_selectionnee)}
<br>
<h1>{$recherche_selectionnee} : </h1>
	{if !empty($result)}
	<textarea id="liste_emails" rows="12">{$result}</textarea><br>
	<button onclick="copier_le_texte()">Copier dans le presse papier</button><br><br>
	Note : l'envoi des emails groupés est généralement limité par les fournisseurs internet à une cinquantaine de destinataires au maximum.
	{else}
	<br>Cette recherche n'a pas trouvé de membres.
	{/if}
{/if}


{include file="admin/_foot.tpl"}
