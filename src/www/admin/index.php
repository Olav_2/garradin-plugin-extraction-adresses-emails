<?php

namespace Garradin;

require_once __DIR__ . '/_inc.php';

use Garradin\Membres\Session;
$session = Session::getInstance();
$user = $session->getUser();
$recherche = new Recherche;

$liste_des_recherches = $recherche->getList($user->id, 'membres'); 
$tpl->assign('liste', $liste_des_recherches);


if (qg('id')) {
	$id = qg('id');

        $recipients = $recherche->search($id, ['id', 'email'], true);

	if ($recipients != null)
	{
		$result = "";
		foreach ($recipients as $recipient) 
		{
			$result .= $recipient->{'email'}.", ";
		}

		$result = substr($result, 0, strlen($result)-2);

		$tpl->assign('result', $result);
	} 

	$recherche_selectionnee = "Cette recherche n'existe pas";

	foreach ($liste_des_recherches as $recherche_iter)
	{
		if ($recherche_iter->{'id'} == $id) 
		{
			$recherche_selectionnee = $recherche_iter->{'intitule'};
		}
	}

	$tpl->assign('recherche_selectionnee', $recherche_selectionnee);
	
}
	
$tpl->display(PLUGIN_ROOT . '/templates/index.tpl');
