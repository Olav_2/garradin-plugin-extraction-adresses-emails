# Plugin d'extraction des emails des membres pour Garradin

Cette extension extrait les adresses emails des membres en fonction des recherches enregistrées, en vue d'envoyer des emails de groupes depuis un client lourd de messagerie ou un webmail.

Exemple d'utilisation : initier des discussions par email entre une partie des membres de l'association, selon leurs centres d'interrêt enregistrés dans leurs fiches de membres.

C'est un complément à la fonctionnalité standard de "message collectif" de Garradin.

# Installation

- Télécharger le fichier extraction_emails.tar.gz et l'ajouter dans le répértoire garradin/plugins.
- Installer le plugin via le menu "Configuration > Extensions" de Garradin.

# Utilisation

- Cliquer sur "emails des membres" dans le menu à gauche de Garradin.
- Cliquer sur l'enveloppe à droite d'une des recherches enregistrées.
- Cliquer sur le bouton "Copier dans le presse papier" en bas de l'écran.
- Coller la liste des emails dans votre logiciel de messagerie ou webmail.

# Droits d'accès

L'écran est accessible uniquement pour les membres ayant au moins le droit de lecture en gestion des membres.

